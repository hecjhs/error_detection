
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void crc();
void codificar();
void verificar_codif();

void resta(int *minuendo,int *sustraendo,int tam);
void division(int *dividendo,int *divisor,int tam_dividendo,int tam_divisor);
void imp(int *num,int tam);

void getbin(string msg, int *bin1,  int  tam_msg );
int getcoef(string msg);
bool ispol(string msg);


void paridad();
void checksum();

void paridad_ascii();

void checksumascii();

int main(int argc, char const *argv[])
{
	int op;

		do{
			system("cls");
			cout<<endl<<endl<<endl;
			cout<<"			Proyecto Redes Basicas "<<endl<<endl<<endl;
			cout<<"		1. CRC"<<endl;
			cout<<"		2. Paridad"<<endl;
			cout<<"		3. Checksum ascii"<<endl;
			cout<<"		4. Salir"<<endl;
			cin>>op;
			switch(op){
				case 1:
				crc();
				break;
				case 2:
				paridad();
				system("pause");
				break;
				case 3:
				checksumascii();
				system("pause");
				break;
				case 4:
				break;
			}
		} while (op!=5);


	return 0;
}
void crc()
{

	int op=0;
	do
	{
	system("cls");
	cout<<endl<<endl<<endl;
	cout<<"				Metodo CRC "<<endl;
	cout<<"		1. Codificar mensaje"<<endl;
	cout<<"		2. Comprobar."<<endl;
	cout<<"		3. Regresar"<<endl;

	cin>>op;
	switch(op){
		case 1:
		system("cls");
			cout<<"Ingrese los datos en binario o en polinomio"<<endl;
			codificar();
		break;
		case 2:
		system("cls");
			cout<<"Ingrese los datos en binario o en polinomio"<<endl;
			verificar_codif();
		break;
		case 3:
		break;
	}
	} while (op!=3);
}


bool val_input(string input)
{
	char simb[13]={'0','1','2','3','4','5','6','7','8','9','x','^','+'};
	bool flag = false;
	for (int i = 0; i < input.size(); ++i){
		for (int j = 0; j < 13; ++j){
			if (input[i]==simb[j]){
				flag = true;
				j=13;
			}else{
				flag = false;
				i=input.size();
				return flag;

			}
		}
	}

	return flag;

}

void codificar()
{
	bool bandera=false;
	int tam_msg, tam_code, tam_codigo;
	int *bin1, *bin2, *codigo, *residuo;
	string msg, code;
	cout<<endl<<endl;
	cout<<"		Introduce el mensaje P(x)"<<endl;
	fflush(stdin);
	getline(cin,msg);
	cout<<endl<<endl;
	cout<<"		Introduce el codigo G(x)"<<endl;
	fflush(stdin);
	getline(cin,code);



	if ( ispol(msg) ){
		system("cls");
		cout<<endl<<endl;
		cout<<"		El mensaje a codificar esta en polinomio"<<endl;
		cout<<"		"<<msg<<endl;
		cout<<"		El mensaje en binario es: "<<endl;
		tam_msg = getcoef(msg) + 1;
		bin1= new int[tam_msg];
		getbin(msg,bin1, tam_msg);

	}
	else{
		system("cls");
		cout<<endl<<endl;
		cout<<"		El mensaje a codificar esta en binario"<<endl;
		tam_msg = msg.size();
		bin1= new int[tam_msg];
		getbin(msg,bin1, tam_msg);
	}
	if (ispol(code)){
		cout<<endl<<endl;
		cout<<"		El codigo generador esta en polinomio"<<endl;
		cout<<"		"<<code<<endl;
		cout<<"		El codigo en binario es: "<<endl;
		tam_code = getcoef(code) + 1;
		bin2= new int[tam_code];
		getbin(code ,bin2, tam_code);
	}
	else{
		cout<<endl<<endl;
		cout<<"		El codigo generador esta en binario"<<endl;
		tam_code = code.size();
		bin2= new int[tam_code];
		getbin(code ,bin2, tam_code);
	}

	//-----------------------------------

	tam_codigo=tam_msg+tam_code-1;
	codigo=new int[tam_codigo];
	residuo=new int[tam_codigo];
	for (int i = 0; i < tam_msg; ++i){
		codigo[i]=bin1[i];
	}
	for(int i=0;i<tam_code-1;i++){
		codigo[tam_msg+i]=0;
	}
	for (int i = 0; i < tam_codigo; ++i)
	{
		residuo[i]=codigo[i];
	}
	division(residuo,bin2,tam_codigo,tam_code);
	cout<<"El codigo verificador es:"<<endl<<endl;
	cout<<" ";
	imp(codigo,tam_codigo);
	cout<<endl<<"-";
	imp(residuo,tam_codigo);
	cout<<endl<<" ";
	for (int i = 0; i < tam_codigo; ++i){
		cout<<"-";
	}
	cout<<endl<<" ";
	resta(codigo,residuo,tam_codigo);
	imp(codigo,tam_codigo);
	cout<<endl<<endl;
	system("pause");
	delete[] codigo;
	delete[] residuo;
}


void verificar_codif()
{
	system ("cls");
	bool bandera=false;
	int tam_msg,tam_code,tam_mensaje;
	int *bin1,*bin2,*residuo,*mensaje;
	string msg, code;
	cout<<endl<<endl;
	cout<<"		Introduce el mensaje P(x)"<<endl;
	fflush(stdin);
	getline(cin,msg);
	cout<<endl<<endl;
	cout<<"		Introduce el codigo G(x)"<<endl;
	fflush(stdin);
	getline(cin,code);


	if ( ispol(msg) ){
		system("cls");
		cout<<endl<<endl<<endl;
		cout<<"		El mensaje es un polinomio"<<endl;
		tam_msg = getcoef(msg) + 1;
		bin1= new int[tam_msg];
		getbin(msg,bin1, tam_msg);

	}
	else{
		cout<<endl<<endl<<endl;
		cout<<"		El mensaje esta en binario"<<endl;
		tam_msg = msg.size();
		bin1= new int[tam_msg];
		getbin(msg,bin1, tam_msg);
	}
	if (ispol(code)){
		cout<<endl<<endl<<endl;
		cout<<"		El codigo es un polinomio"<<endl;
		tam_code = getcoef(code) + 1;
		bin2= new int[tam_code];
		getbin(code ,bin2, tam_code);
	}
	else{
		cout<<endl<<endl<<endl;
		cout<<"		El codigo esta en binario"<<endl;
		tam_code = code.size();
		bin2= new int[tam_code];
		getbin(code ,bin2, tam_code);
	}

	tam_mensaje=tam_msg-tam_code+1;
	residuo=new int[tam_msg];
	for (int i = 0; i < tam_msg; ++i){
		residuo[i]=bin1[i];
	}

	division(residuo,bin2,tam_msg,tam_code);
	for (int i = 0; i < tam_msg; ++i)
	{
		if(residuo[i]){
			bandera=true;
		}
	}
	if(bandera){
		cout<<"		El codigo verificador contiene errores."<<endl;
		for(int i=0;i<tam_code-1;i++){
			bin1[tam_mensaje+i]=0;
		}
		for (int i = 0; i < tam_msg; ++i)
		{
			residuo[i]=bin1[i];
		}
		division(residuo,bin2,tam_msg,tam_code);
		cout<<"		El codigo verificador corregido es:"<<endl;
		cout<<"		";
		resta(bin1,residuo,tam_msg);
		imp(bin1,tam_msg);
		cout<<endl<<endl;
		cout<<"		Ahora se vuelve a transmitir:"<<endl;
		cout<<"		";
		imp(bin1,tam_msg);
		cout<<endl<<endl;
		cout<<"		El mensaje original es:"<<endl;
		cout<<"		";
		imp(bin1,tam_mensaje);
		cout<<endl<<endl;
		cout<<"		ComprobaciOn: "<<endl;
		cout<<endl<<endl;
		division(bin1,bin2,tam_msg,tam_code);


	}
	else{

		cout<<"El codigo verificador se ha transmitido correctamente."<<endl<<"El mensaje es:"<<endl;
		imp(bin1,tam_mensaje);
		cout<<endl<<endl;
	}
	system("pause");
	delete[] residuo;
}
bool ispol(string msg){
	if (msg[0]=='x'){
		return true;
	}
	else
		return false;
}
void getbin(string msg, int *bin1, int  tam_msg){

	int num;
	if (msg[0]=='x'){
		for (int i = 0; i < tam_msg; ++i){
			bin1[i]=0;
		}
		for(int i=0; i<msg.size(); i++){
			if ( isdigit(msg[i]) && isdigit(msg[i+1]) ){
				num = (msg[i] - 48)*10 + (msg[i+1] - 48);
				bin1[num]=1;
				i=i+2;
			}if( isdigit(msg[i]) ) {

				num = (msg[i] - 48);

				if(num==1 && msg[i-1] =='+' ){
					num=0;
				}

				bin1[num]=1;

			}
		}
		cout<<"		";
		for (int i = tam_msg -1 ; i >= 0; --i){
			cout<<bin1[i];
		}

	}
	else{

		for (int i = 0; i < tam_msg; ++i){
			bin1[i]=0;
		}

		for (int i = 0; i < tam_msg; ++i){
			bin1[i]= (msg[ i ] - 48 );
		}
		cout<<"		";
		for (int i = 0; i < tam_msg; ++i){

			cout<<bin1[i];
		}
		cout<<endl;
	}

}
void resta(int *minuendo,int *sustraendo,int tam1){
	for (int i = 0; i < tam1; ++i){
		minuendo[i]=minuendo[i]^sustraendo[i];
	}
}
void division(int *dividendo,int *divisor,int tam_dividendo,int tam_divisor){
	cout<<endl<<endl<<"		Operaciones: "<<endl<<endl;
	int *sustraendo=new int[tam_dividendo];
	int cont=0;
	for (int i = 0; i < tam_dividendo; ++i){
		sustraendo[i]=0;
	}
	for (int i = 0; i < tam_divisor; ++i){
		sustraendo[i]=divisor[i];
	}
	cout<<"		Se divide: ";
	imp(dividendo,tam_dividendo);
	cout<<"  entre:  ";
	imp(divisor,tam_divisor);
	cout<<endl;
	cout<<" ";
	imp(dividendo,tam_dividendo);
	cout<<endl;
	for (int i = 0; i < tam_dividendo-tam_divisor+1; ++i)
	{
		int j;
		if(dividendo[i])
		{
			resta(dividendo,sustraendo,tam_dividendo);
			j=0;
			while(sustraendo[j]!=1){
				cout<<" ";
				j++;
			}
			cout<<" ";
			imp(divisor,tam_divisor);
			cout<<endl<<" ";// linea
			/*
			for (j = 0; j < tam_dividendo; ++j){
				cout<<"-";
			}*/
			cout<<endl<<" ";
			bool bandera1=true;
			int k=0;
			for (j = 0; j < tam_dividendo; ++j)
			{
				if (dividendo[j]==1 && k==0)
				{
					bandera1=false;
				}
				if (bandera1)
				{
					cout<<" ";
				}
				else
				{
					cout<<dividendo[j];
					k++;
					if (k==tam_divisor)
					{
						bandera1=true;
					}
				}
			}
			if (bandera1&&k==0)
			{
				cout<<"0";
			}
			if (cont== tam_dividendo-tam_divisor || cont== tam_dividendo-tam_divisor+1)
			{
				cout<<" <---- Residuo !!!";
			}


			cout<<endl;
		}
		for (j = tam_dividendo-1; j > 0; j--)
		{
			sustraendo[j]=sustraendo[j-1];
		}
		sustraendo[0]=0;

		cont++;
	}

	cout<<endl<<endl;
}
void imp(int *num,int tam1){
	for (int i = 0; i < tam1; ++i)
	{
		cout<<num[i];
	}
}
int getcoef(string msg){
	int num;
	if ( isdigit(msg[2]) && isdigit(msg[3])){
		num = (msg[2] - 48)*10 + (msg[3] - 48);
		return num;
	}else
	num= msg[2] - 48;
	return num;
}
//------------ Paridad -----
void paridad()
{
	system("cls");
	bool bandera=false;
	int tam_msg;
	int *bin1;
	int *nbin;
	int cont, cont2=0;
	string msg;
	cout<<endl<<endl;
	cout<<"			Introduce el mensaje P(x)"<<endl;
	fflush(stdin);
	getline(cin,msg);

	if ( ispol(msg) ){
		cout<<"		El mensaje a codificar esta en polinomio"<<endl;
		tam_msg = getcoef(msg) + 1;
		bin1= new int[tam_msg];
		getbin(msg,bin1, tam_msg);

	}
	else{
		cout<<		"El mensaje a codificar esta en binario"<<endl;
		tam_msg = msg.size();
		bin1= new int[tam_msg];
		getbin(msg,bin1, tam_msg);
	}
	//-----------------------------------
	for (int i = 0; i < tam_msg; ++i){
		if (bin1[i]==1){
			cont++;
		}
	}
	cout<<endl<<endl;
	cout<<"		El numero de 1's es: "<<cont<<endl;
	if (cont%2==0){
		cout<<"		El numero de 1 es par por lo tanto se agregara un 0 al mensaje"<<endl;
		nbin=new int[tam_msg+1];

		for (int i = 0; i < tam_msg; ++i){
			nbin[i]=bin1[i];

		}
		nbin[tam_msg]=0;
		cout<<"		Paridad "<<endl<<endl;
		for (int i = 0; i < tam_msg+1; ++i){
			cout<<nbin[i];
		}
		cout<<endl;
	}else{
		cout<<"		El numero de 1 es impar por lo tanto se agregara un 1 al mensaje"<<endl;
		nbin=new int[tam_msg+1];

		for (int i = 0; i < tam_msg; ++i){
			nbin[i]=bin1[i];

		}
		nbin[tam_msg]=1;
		cout<<"		Paridad"<<endl<<endl;
		for (int i = 0; i < tam_msg+1; ++i){
			cout<<nbin[i];
			if ( nbin [i] == 1){
			cont2++;
		}

		}
		cout<<endl;
	}
	cout<<endl<<endl;
	cout<<"		El mensaje se ha transmitido sin errores"<<endl;
	cout<<"		El receptor recibe: "<<endl;
		for (int i = 0; i < tam_msg; ++i){
			cout<<nbin[i];
		}
		cout<<endl;
}

void paridad_ascii(){

	string msg;
	cout<<"Introduzca el mensaje"<<endl;
	fflush(stdin);
	getline(cin, msg);
	for (int i = 0; i < msg.size(); ++i){
		cout<<msg[i];
	}
	cout<<endl;



}


void checksumascii(){

 	string msg;
 	int sum=0;
 	int num, tam_msg,com;
 	bool flag=true;
 	system("cls");
 	cout<<endl<<endl;
	cout<<"			Introduce el mensaje"<<endl;
	fflush(stdin);
	getline(cin,msg);
	tam_msg=msg.size();
	for (int i = 0; i < tam_msg; ++i){
		num=msg[i];
		cout<<"   "<<msg[i]<<"	+	"<<num<<endl;
		sum=sum+num;
	}

	cout<<"----------------------------"<<endl;
	cout<<"Checksum = 	"<<sum<<endl;
	cout<<"Se envia el mensaje + "<<sum<<endl;
	system("pause");
	cout<<endl<<endl;
	do{
		cout<<"Comprobar checksum"<<endl<<
		endl<<"Introduzca el valor del checksum obtenido"<<endl;
		cin>>com;
		cout<<"Introduzca el mensaje"<<endl;
		fflush(stdin);
		getline(cin,msg);
		tam_msg=msg.size();
		sum=0;
		for (int i = 0; i < tam_msg; ++i){
			num=msg[i];
			sum=sum+num;
		}
		if (sum == com){
			cout<<"El mensaje se ha transmitido correctamente"<<endl;
			flag=false;
		}else{
			cout<<"Los checksum no coenciden"<<endl;
			cout<<"Se han generado errores"<<endl;
		}
	}while(flag);
}
